![vital-5g-logo](https://www.vital5g.eu/wp-content/uploads/2020/12/vital-logo-web.png) 
 

# VITAL-5G Platform installation
This repository contains a docker compose and docker files aiming to ease the build and deployment process of the VITAL-5G Platform core components:
* [Catalogue](https://gitlab.com/vital-5g/VITAL-5G_Platform/vital-5g-catalogue)
* [Multi-site inventory](https://gitlab.com/vital-5g/VITAL-5G_Platform/multi-site-inventory)
* [Multi-site 5G slice inventory](https://gitlab.com/vital-5g/VITAL-5G_Platform/multi-site-5g-slice-inventory)
* [Service LCM](https://gitlab.com/vital-5g/VITAL-5G_Platform/service-lcm)
* [Experiment LCM](https://gitlab.com/vital-5g/VITAL-5G_Platform/experiment-lcm)
* [Monitoring Platform](https://gitlab.com/vital-5g/VITAL-5G_Platform/monitoring-platform)
* [Portal Web GUI](https://gitlab.com/vital-5g/VITAL-5G_Platform/portal-web-gui)

# Prerequisites

The only prerequisite is a server running docker

# Deploy


	git clone https://gitlab.com/vital-5g/VITAL-5G_Platform/vital-5g-platform-installation.git
	cd vital-5g-platform-installation && docker compose up -d --no-build .


# Build (optional)

Some of the components are released as open source, and therefore the code is available to be built from scratch using:
	
	docker compose build .
	

# Maintainers

* Technical Queries contact: Juan Brenes (Nextworks) -  j.brenes@nextworks.it  
* General Queries:  info@vital.eu


